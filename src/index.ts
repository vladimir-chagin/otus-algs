import runSquares  from './01-recursion-and-loops/squares';
import runTickets from './01-recursion-and-loops/tickets-01';
import runLengths from './01-recursion-and-loops/lengths-00';

const main = async () => {
    await runLengths();
    // await runTickets();
    // await runSquares();
};

(async () => {
    await main();
})();

