import fsp from 'fs/promises';
import path from "path";
import readline from 'readline';
import * as fs from "fs";

enum EXTENSIONS {
    IN = '.in',
    OUT = '.out',
}

const extractNumber = (fileName: string): number => {
    return parseInt(fileName.replace(/[^0-9]/g, ''));
}

type TaskEntry = {
    inputFilePath: string,
    outputFilePath: string,
    fileName: string,
};

const getTasks = async (pathToDir: string): Promise<TaskEntry[]> => {
    pathToDir = path.resolve(process.env.INIT_CWD as string, pathToDir);
    const allFiles = await fsp.readdir(pathToDir, {
        withFileTypes: true,
    });

    const taskEntries: TaskEntry[] = [];

    for (const entry of allFiles) {
        if (!entry.isFile()) {
            continue;
        }

        const idx = extractNumber(entry.name);
        if (isNaN(idx) || idx < 0) {
            continue;
        }

        const taskEntry = taskEntries[idx] || {};
        taskEntry.fileName = entry.name;
        const pathToFile = path.resolve(pathToDir, entry.name);
        if (entry.name.endsWith(EXTENSIONS.IN)) {
            taskEntry.inputFilePath = pathToFile;
        } else if (entry.name.endsWith(EXTENSIONS.OUT)) {
            taskEntry.outputFilePath = pathToFile;
        }
        taskEntries[idx] = taskEntry;
    }
    return taskEntries;
}

const readAllLinesFromFile = async (pathToFile: string): Promise<string[]> => {
    const lines: string[] = [];
    const input = fs.createReadStream(pathToFile, {
        encoding: "utf-8"
    });
    const lineReader = readline.createInterface({
        input,
        crlfDelay: Infinity
    });
    for await (const line of lineReader) {
        lines.push(line);
    }
    return lines;
};

const readStringFromFile = async (pathToFile: string): Promise<string> => {
    const fh = await fsp.open(pathToFile, 'r');
    try {
        const result = await fh.readFile({
            encoding: "utf-8"
        });
        return (result || '').trim();
    } finally {
        await fh.close();
    }
};

const readNumberFromFile = async (pathToFile: string): Promise<number> => {
    const str = await readStringFromFile(pathToFile);
    return parseInt(str);
};

export default {
    getTasks,
    readAllLinesFromFile,
    readStringFromFile,
    readNumberFromFile,
}