import io from "./io";

export interface ITask {
    run: (data: string[]) => string;
    runAsync: (data: string[]) => Promise<string>;
}

const TaskRunner = {
    async runTask(pathToTaskDir: string, task: ITask, runAsync=false): Promise<void> {
        const taskEntries = await io.getTasks(pathToTaskDir);

        for (const taskDescriptor of taskEntries) {
            const taskInputs = await io.readAllLinesFromFile(taskDescriptor.inputFilePath);
            const expectedTaskOutput = await io.readStringFromFile(taskDescriptor.outputFilePath);

            const actualTaskOutput = runAsync ? await task.runAsync(taskInputs) : task.run(taskInputs);

            const equals = actualTaskOutput === expectedTaskOutput;
            if (equals) {
                console.log(`${taskDescriptor.fileName}: ok`);
            } else {
                console.warn(`${taskDescriptor.fileName}: actual: ${actualTaskOutput}, expected: ${expectedTaskOutput}`);
            }
        }
    }
};

export default TaskRunner;