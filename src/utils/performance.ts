import { performance } from 'perf_hooks';

const measure = async (callback: () => Promise<any> | void): Promise<void> => {
    const start = performance.now();
    const result = callback();
    if (result) {
        await result;
    }
    const duration = performance.now() - start;
};

export default {
    measure,
}