const A = 25;   //square side length

type Condition = (x: number, y: number) => boolean;

const squares = (cond: Condition) => {
    for (let x = 0; x < A; x += 1) {
        let line = [];
        for (let y = 0; y < A; y += 1) {
            line.push(cond(x, y) ? '#' : '.');
            line.push(' ');
        }
        console.log(line.join(''));
    }
};

const c: Condition[] = [];
c[1] = (x, y) => x < y;
c[2] = (x, y) => x === y;
c[3] = (x, y) => x === A-y-1;
c[4] = (x, y) => x - 5 <= A + 5 - y;
c[5] = (x, y) => x === Math.floor(y / 2);
c[6] = (x, y) => x <= 10 || y <= 10;
c[7] = (x, y) => x >= 9 && y >= 9;
c[8] = (x, y) => x * y === 0;
c[9] = (x, y) => x - 14 < y - A || y - 14 < x - A;
c[10] = (x, y) => y - 1 >= x && y - 1 <= 2 * x;
c[11] = (x, y) => (x - 1) * (y - 1) * (A - x - 2) * (A - y - 2)=== 0;
c[12] = (x, y) => x * x + y * y <= (A - 4) * (A - 4);
c[13] = (x, y) => x + y < A + 5  && x + y >= A - 5;
c[14] = (x, y) => y <= Math.floor(6 * A / (x + 1));
c[15] = (x, y) => (x - y < 20 - A && x - y >= 9 - A) || (y - x < 14 - A && y - x > 4 - A);
c[16] = (x, y) => (x <= y + 9) && (x > A - 9 - y) && (x < A + 7 - y) && (x >= y - 7);
c[17] = (x, y) => x >= 1.75*A - Math.floor(A*Math.sin(Math.PI*y/(A)));
c[18] = (x, y) => (y < 2 || x < 2) && (x + y > 0);
c[19] = (x, y) => x * y * (x - A + 1) * (y - A + 1) === 0;
c[20] = (x, y) => (x + y) % 2 === 0;
c[21] = (x, y) => false;//(x * y) * (x - y + 1) * (3*x - y + 1) * (5*x-y) === 0;
c[22] = (x, y) => (x + y) % 3 === 0;
c[23] = (x, y) => (y % 2) === 0 && (x % 3) === 0;
c[24] = (x, y) => (x-y) * (x + y - A + 1) === 0;
c[25] = (x, y) => (x % 6) * (y % 6) === 0;

const run = (n?: number) => {
    if (typeof(n) === 'number') {
        console.log(n);
        squares(c[n]);
    } else {
        for (const cond of c) {
            if (cond) {
                squares(cond);
                console.log('');
            }
        }
    }
};

export default run;


