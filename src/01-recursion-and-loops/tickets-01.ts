import TaskRunner, {ITask} from "../utils/TaskRunner";

const pathToDir = './tasks/01-recursion-and-loops/1.Tickets';

function calcMagicNumbersAmountRecursive(n: number, k: number, sum1:number, sum2: number): number {
    if (n === k) {
        return 1;
    }

    let cnt = 0;
    if (k < n / 2) {
        for (let i = 0; i < 10; i += 1) {
            cnt += calcMagicNumbersAmountRecursive(n, k + 1, sum1 + i, sum2);
        }
    } else {
        for (let i = 0; i < 10; i += 1) {
            cnt += calcMagicNumbersAmountRecursive(n, k + 1, sum1, sum2 + i);
        }
    }

    return cnt;
}

const task: ITask = {
    run(data) {
        const n = parseInt(data[0]);
        return '' + calcMagicNumbersAmountRecursive(n, 0, 0, 0);
    },
    async runAsync(data) {
        return '';
    }
};

const run = async () => {
    await TaskRunner.runTask(pathToDir, task);
};

export default run;

