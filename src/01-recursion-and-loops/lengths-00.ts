import TaskRunner, { ITask } from "../utils/TaskRunner";

const stringLength = (input: string): string => {
    return input.trim().length.toString();
};

const pathToDir = './tasks/01-recursion-and-loops/0.String';

const task: ITask = {
    run(data) {
        return stringLength(data[0]);
    },
    async runAsync(data) {
        return '';
    }

};

const run = async () => {
    await TaskRunner.runTask(pathToDir, task);
};

export default run;

